<?php

namespace Drupal\vc_events\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for VC Event entities.
 */
class VCEventViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
