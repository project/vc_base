<?php

namespace Drupal\vc_events\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining VC Event type entities.
 */
interface VCEventTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
