<?php

namespace Drupal\vc_resources;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\vc_resources\Entity\VCResourceInterface;

/**
 * Defines the storage handler class for VCResource entities.
 *
 * This extends the base storage class, adding required special handling for
 * VCResource entities.
 *
 * @ingroup vc_resources
 */
interface VCResourceStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of VCResource revision IDs for a specific VCResource.
   *
   * @param \Drupal\vc_resources\Entity\VCResourceInterface $entity
   *   The VCResource entity.
   *
   * @return int[]
   *   VCResource revision IDs (in ascending order).
   */
  public function revisionIds(VCResourceInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as VCResource author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   VCResource revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\vc_resources\Entity\VCResourceInterface $entity
   *   The VCResource entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(VCResourceInterface $entity);

  /**
   * Unsets the language for all VCResource with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
