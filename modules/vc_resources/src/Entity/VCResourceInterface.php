<?php

namespace Drupal\vc_resources\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining VCResource entities.
 *
 * @ingroup vc_resources
 */
interface VCResourceInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the VCResource name.
   *
   * @return string
   *   Name of the VCResource.
   */
  public function getName();

  /**
   * Sets the VCResource name.
   *
   * @param string $name
   *   The VCResource name.
   *
   * @return \Drupal\vc_resources\Entity\VCResourceInterface
   *   The called VCResource entity.
   */
  public function setName($name);

  /**
   * Gets the VCResource creation timestamp.
   *
   * @return int
   *   Creation timestamp of the VCResource.
   */
  public function getCreatedTime();

  /**
   * Sets the VCResource creation timestamp.
   *
   * @param int $timestamp
   *   The VCResource creation timestamp.
   *
   * @return \Drupal\vc_resources\Entity\VCResourceInterface
   *   The called VCResource entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the VCResource revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the VCResource revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\vc_resources\Entity\VCResourceInterface
   *   The called VCResource entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the VCResource revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the VCResource revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\vc_resources\Entity\VCResourceInterface
   *   The called VCResource entity.
   */
  public function setRevisionUserId($uid);

}
