<?php

namespace Drupal\vc_resources;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for vc_resource.
 */
class VCResourceTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
