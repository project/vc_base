<?php

namespace Drupal\vc_resources\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\vc_resources\Entity\VCResourceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class VCResourceController.
 *
 *  Returns responses for VCResource routes.
 */
class VCResourceController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a VCResource revision.
   *
   * @param int $vc_resource_revision
   *   The VCResource revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($vc_resource_revision) {
    $vc_resource = $this->entityTypeManager()->getStorage('vc_resource')
      ->loadRevision($vc_resource_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('vc_resource');

    return $view_builder->view($vc_resource);
  }

  /**
   * Page title callback for a VCResource revision.
   *
   * @param int $vc_resource_revision
   *   The VCResource revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($vc_resource_revision) {
    $vc_resource = $this->entityTypeManager()->getStorage('vc_resource')
      ->loadRevision($vc_resource_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $vc_resource->label(),
      '%date' => $this->dateFormatter->format($vc_resource->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a VCResource.
   *
   * @param \Drupal\vc_resources\Entity\VCResourceInterface $vc_resource
   *   A VCResource object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(VCResourceInterface $vc_resource) {
    $account = $this->currentUser();
    $vc_resource_storage = $this->entityTypeManager()->getStorage('vc_resource');

    $langcode = $vc_resource->language()->getId();
    $langname = $vc_resource->language()->getName();
    $languages = $vc_resource->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $vc_resource->label()]) : $this->t('Revisions for %title', ['%title' => $vc_resource->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all vc_resource revisions") || $account->hasPermission('administer vc_resource entities')));
    $delete_permission = (($account->hasPermission("delete all vc_resource revisions") || $account->hasPermission('administer vc_resource entities')));

    $rows = [];

    $vids = $vc_resource_storage->revisionIds($vc_resource);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\vc_resources\VCResourceInterface $revision */
      $revision = $vc_resource_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $vc_resource->getRevisionId()) {
          $link = $this->l($date, new Url('entity.vc_resource.revision', [
            'vc_resource' => $vc_resource->id(),
            'vc_resource_revision' => $vid,
          ]));
        }
        else {
          $link = $vc_resource->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.vc_resource.translation_revert', [
                'vc_resource' => $vc_resource->id(),
                'vc_resource_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.vc_resource.revision_revert', [
                'vc_resource' => $vc_resource->id(),
                'vc_resource_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.vc_resource.revision_delete', [
                'vc_resource' => $vc_resource->id(),
                'vc_resource_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['vc_resource_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
