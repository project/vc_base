<?php

namespace Drupal\vc_resources\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting VCResource entities.
 *
 * @ingroup vc_resources
 */
class VCResourceDeleteForm extends ContentEntityDeleteForm {


}
