<?php

namespace Drupal\vc_resources\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a VCResource revision.
 *
 * @ingroup vc_resources
 */
class VCResourceRevisionDeleteForm extends ConfirmFormBase {

  /**
   * The VCResource revision.
   *
   * @var \Drupal\vc_resources\Entity\VCResourceInterface
   */
  protected $revision;

  /**
   * The VCResource storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $vCResourceStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->vCResourceStorage = $container->get('entity_type.manager')->getStorage('vc_resource');
    $instance->connection = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vc_resource_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => format_date($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.vc_resource.version_history', ['vc_resource' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $vc_resource_revision = NULL) {
    $this->revision = $this->VCResourceStorage->loadRevision($vc_resource_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->VCResourceStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')->notice('VCResource: deleted %title revision %revision.', ['%title' => $this->revision->label(), '%revision' => $this->revision->getRevisionId()]);
    $this->messenger()->addMessage(t('Revision from %revision-date of VCResource %title has been deleted.', ['%revision-date' => format_date($this->revision->getRevisionCreationTime()), '%title' => $this->revision->label()]));
    $form_state->setRedirect(
      'entity.vc_resource.canonical',
       ['vc_resource' => $this->revision->id()]
    );
    if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {vc_resource_field_revision} WHERE id = :id', [':id' => $this->revision->id()])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.vc_resource.version_history',
         ['vc_resource' => $this->revision->id()]
      );
    }
  }

}
