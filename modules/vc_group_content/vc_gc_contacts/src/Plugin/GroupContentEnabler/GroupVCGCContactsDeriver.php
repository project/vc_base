<?php

namespace Drupal\vc_gc_contacts\Plugin\GroupContentEnabler;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\vc_contacts\Entity\VCContactType;

class GroupVCGCContactsDeriver extends DeriverBase {

  /**
   * {@inheritdoc}.
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach (VCContactType::loadMultiple() as $name => $vc_contact_type) {
      $label = $vc_contact_type->label();

      $this->derivatives[$name] = [
        'entity_bundle' => $name,
        'label' => t('Group contact (@type)', ['@type' => $label]),
        'description' => t('Adds %type content to groups both publicly and privately.', ['%type' => $label]),
      ] + $base_plugin_definition;
    }

    return $this->derivatives;
  }

}
