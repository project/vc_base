<?php

/**
 * @file
 * Contains vc_contact.page.inc.
 *
 * Page callback for VCContact entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for VCContact templates.
 *
 * Default template: vc_contact.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_vc_contact(array &$variables) {
  // Fetch VCContact Entity Object.
  $vc_contact = $variables['elements']['#vc_contact'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
