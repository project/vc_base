<?php

namespace Drupal\vc_contacts_volunteers;

use Drupal\Core\Entity\EntityInterface;

class VCContactVolunteersUtilities {

  /**
   * @param EntityInterface $account
   * @param bool $id
   * @return bool|\Drupal\Core\Entity\EntityInterface|mixed
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function getProfileFromUser(EntityInterface $account, $id = FALSE) {
    $params = [
      'type' => 'volunteer',
      'field_associate_user' => $account->id()
    ];
    $profiles = \Drupal::entityTypeManager()->getStorage('vc_contact')->loadByProperties($params);
    if ($profiles) {
      $profile = reset($profiles);
      return $id ? $profile->id() : $profile;
    }
    return FALSE;
  }

}