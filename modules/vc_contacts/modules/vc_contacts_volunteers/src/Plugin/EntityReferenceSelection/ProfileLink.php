<?php

namespace Drupal\vc_contacts_volunteers\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;
use Drupal\Core\Form\FormStateInterface;


/**
 * Defines an alternative to the default Entity Reference Selection plugin.
 *
 * @EntityReferenceSelection(
 *   id = "vc_contact_profile:vc_contact",
 *   label = @Translation("VC: Link to Profile"),
 *   group = "vc_contact_profile",
 *   entities = {"vc_contact"},
 *   weight = 1,
 *   base_plugin_label = @Translation("VC Contact: All Items or profile query parameter")
 * )
 */
class ProfileLink extends DefaultSelection {

  public function getReferenceableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {
    $list = parent::getReferenceableEntities($match, $match_operator, $limit);
    $profile_query = \Drupal::request()->query->get('profile', 0);
    if (!$profile_query) {
      $user_query = \Drupal::request()->query->get('user', 0);
      if ($user_query) {
        $query = \Drupal::entityQuery('vc_contact');
        $query->condition('type', 'volunteer');
        $query->condition('field_associate_user', $user_query);
        $result = $query->execute();
        $profile_query = array_shift($result);
        $profile_query = $profile_query ? $profile_query : 0;
      }
    }

    $return_list = [];
    foreach ($list as $bundle => $bundle_list) {
      if (isset($bundle_list[$profile_query])) {
        $return_list[$bundle] = [$profile_query => $bundle_list[$profile_query]];
      }
    }

    return !empty($return_list) ? $return_list : $list;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $configuration = $this->getConfiguration();
    $entity_type_id = $configuration['target_type'];
    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    $bundles = $this->entityTypeBundleInfo->getBundleInfo($entity_type_id);

    if ($entity_type->hasKey('bundle')) {
      $bundle_options = [];
      foreach ($bundles as $bundle_name => $bundle_info) {
        $bundle_options[$bundle_name] = $bundle_info['label'];
      }
      natsort($bundle_options);

      $form['target_bundles'] = [
        '#type' => 'checkboxes',
        '#title' => $entity_type->getBundleLabel(),
        '#description' => $this->t('Select applicable bundles, or leave blank for all bundles.'),
        '#options' => $bundle_options,
        '#default_value' => (array) $configuration['target_bundles'],
        '#required' => FALSE,
        '#size' => 6,
        '#multiple' => TRUE,
        '#element_validate' => [[get_class($this), 'elementValidateFilter']],
        '#ajax' => TRUE,
        '#limit_validation_errors' => [],
      ];
    }
    else {
      $form['target_bundles'] = [
        '#type' => 'value',
        '#value' => [],
      ];
    }

    return $form;
  }

}
