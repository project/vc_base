<?php

namespace Drupal\vc_contacts;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for vc_contact.
 */
class VCContactTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
