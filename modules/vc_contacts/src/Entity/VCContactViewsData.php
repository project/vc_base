<?php

namespace Drupal\vc_contacts\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for VCContact entities.
 */
class VCContactViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
