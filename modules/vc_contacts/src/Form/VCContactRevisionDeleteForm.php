<?php

namespace Drupal\vc_contacts\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a VCContact revision.
 *
 * @ingroup vc_contacts
 */
class VCContactRevisionDeleteForm extends ConfirmFormBase {

  /**
   * The VCContact revision.
   *
   * @var \Drupal\vc_contacts\Entity\VCContactInterface
   */
  protected $revision;

  /**
   * The VCContact storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $vCContactStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->vCContactStorage = $container->get('entity_type.manager')->getStorage('vc_contact');
    $instance->connection = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vc_contact_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => format_date($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.vc_contact.version_history', ['vc_contact' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $vc_contact_revision = NULL) {
    $this->revision = $this->VCContactStorage->loadRevision($vc_contact_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->VCContactStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')->notice('VCContact: deleted %title revision %revision.', ['%title' => $this->revision->label(), '%revision' => $this->revision->getRevisionId()]);
    $this->messenger()->addMessage(t('Revision from %revision-date of VCContact %title has been deleted.', ['%revision-date' => format_date($this->revision->getRevisionCreationTime()), '%title' => $this->revision->label()]));
    $form_state->setRedirect(
      'entity.vc_contact.canonical',
       ['vc_contact' => $this->revision->id()]
    );
    if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {vc_contact_field_revision} WHERE id = :id', [':id' => $this->revision->id()])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.vc_contact.version_history',
         ['vc_contact' => $this->revision->id()]
      );
    }
  }

}
