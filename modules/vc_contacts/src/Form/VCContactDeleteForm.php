<?php

namespace Drupal\vc_contacts\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting VCContact entities.
 *
 * @ingroup vc_contacts
 */
class VCContactDeleteForm extends ContentEntityDeleteForm {


}
