<?php

namespace Drupal\vc_contacts;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\vc_contacts\Entity\VCContactInterface;

/**
 * Defines the storage handler class for VCContact entities.
 *
 * This extends the base storage class, adding required special handling for
 * VCContact entities.
 *
 * @ingroup vc_contacts
 */
class VCContactStorage extends SqlContentEntityStorage implements VCContactStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(VCContactInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {vc_contact_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {vc_contact_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(VCContactInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {vc_contact_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('vc_contact_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
