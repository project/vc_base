<?php

namespace Drupal\vc_actions_volunteer\Access;

use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\Routing\Route;

/**
 * Class NewVolunteer
 * @package Drupal\vc_actions_volunteer\Access
 */
class VolunteerPendingStatus implements AccessInterface {

  /**
   * Check whether the person has volunteered already.
   *
   * @param Route $route
   * @param RouteMatchInterface $route_match
   * @param AccountInterface $account
   * @return AccessResult|\Drupal\Core\Access\AccessResultAllowed
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {

    $vc_action = $route_match->getParameter('vc_action');
    $status = $vc_action->get('field_action_status')->getValue();
    $status = isset($status[0]['value']) ? $status[0]['value'] : $status;
    if ($status === 'request') {
      return AccessResult::allowedIfHasPermission($account,'vc_actions_volunteer manage volunteer requests');
    }
    return AccessResult::forbidden();

  }
}
