<?php

namespace Drupal\vc_actions_volunteer;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\vc_needs\Entity\VCNeed;

trait VCActionsVolunteerTrait {

  public function checkMaxVolunteers (VCNeed $vc_need, AccountInterface $account) {
    $max_volunteers = $vc_need->get('field_max_volunteers')->getValue();
    $max_volunteers = isset($max_volunteers[0]['value']) ? $max_volunteers[0]['value'] : $max_volunteers;
    // Access is allowed if there is no max, or less than the total maximum.
    if (!empty($max_volunteers)) {
      $query = \Drupal::entityQuery('vc_action');
      $query->condition('type', 'volunteer_action');
      $query->condition('field_need', $vc_need->id());
      $query->condition('field_action_status', ['request','cancel','complete','cancel_volunteer'], 'NOT IN');
      $query->condition('status', 1);
      $result = $query->execute();
      if (count($result) >= $max_volunteers) {
        return FALSE;
      }
    }
    return TRUE;
  }

  public function checkNewVolunteer(VCNeed $vc_need, AccountInterface $account) {
    // First check if the user has a profile.
    $profile_query = \Drupal::entityQuery('vc_contact');
    $profile_query->condition('type', 'volunteer');
    $profile_query->condition('field_associate_user', $account->id());
    $result = $profile_query->execute();
    $profile = array_shift($result);
    if (!empty($profile)) {
      $query = \Drupal::entityQuery('vc_action');
      $query->condition('type', 'volunteer_action');
      $query->condition('field_need', $vc_need->id());
      $query->condition('field_link_to_profile', $profile);
      $query->condition('status', 1);
      $result = $query->execute();
      if (empty($result)) {
        return TRUE;
      }
    }
    return FALSE;
  }

}