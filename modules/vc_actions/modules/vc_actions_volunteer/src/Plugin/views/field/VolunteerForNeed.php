<?php

namespace Drupal\vc_actions_volunteer\Plugin\views\field;

use Drupal\Component\Utility\Xss;
use Drupal\vc_actions_volunteer\VCActionsVolunteerTrait;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\views_add_button\Plugin\views_add_button\ViewsAddButtonDefault;
use Drupal\views_add_button\ViewsAddButtonUtilities;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;

/**
 * Defines a views field plugin.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("vc_actions_volunteer_for_need")
 */
class VolunteerForNeed extends FieldPluginBase {

  use VCActionsVolunteerTrait;

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * Define the available options.
   *
   * @return array
   *   Array of available options for views_add_button form.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    return $options;
  }

  /**
   * Provide the options form.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $account = \Drupal::currentUser();
    $max = $this->checkMaxVolunteers($values->_entity, $account);
    $new = $this->checkNewVolunteer($values->_entity, $account);
    if (!$new) {
      return ['#markup' => 'You have volunteered for this need.'];
    }
    if (!$max) {
      return ['#markup' => 'This need has the maximum number of users.'];
    }
    if (!$account->hasPermission('vc_actions_volunteer volunteer for need')) {
      return ['#markup' => ''];
    }
    $volunteer_url = Url::fromRoute('vc_actions_volunteer.volunteer', ['vc_need' => $values->_entity->id()]);
    $volunteer_link = Link::fromTextAndUrl('Volunteer', $volunteer_url);
    return $volunteer_link->toRenderable();
  }

}
