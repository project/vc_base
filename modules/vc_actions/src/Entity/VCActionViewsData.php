<?php

namespace Drupal\vc_actions\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for VCAction entities.
 */
class VCActionViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
