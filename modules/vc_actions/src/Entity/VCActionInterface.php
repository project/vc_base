<?php

namespace Drupal\vc_actions\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining VCAction entities.
 *
 * @ingroup vc_actions
 */
interface VCActionInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the VCAction name.
   *
   * @return string
   *   Name of the VCAction.
   */
  public function getName();

  /**
   * Sets the VCAction name.
   *
   * @param string $name
   *   The VCAction name.
   *
   * @return \Drupal\vc_actions\Entity\VCActionInterface
   *   The called VCAction entity.
   */
  public function setName($name);

  /**
   * Gets the VCAction creation timestamp.
   *
   * @return int
   *   Creation timestamp of the VCAction.
   */
  public function getCreatedTime();

  /**
   * Sets the VCAction creation timestamp.
   *
   * @param int $timestamp
   *   The VCAction creation timestamp.
   *
   * @return \Drupal\vc_actions\Entity\VCActionInterface
   *   The called VCAction entity.
   */
  public function setCreatedTime($timestamp);

}
