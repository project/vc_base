<?php

/**
 * @file
 * Contains vc_needs.install.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Add "Active" property to report-related entities, and set to reportable initially.
 */
function vc_needs_update_8101(&$sandbox) {
  $definition_manager = \Drupal::entityDefinitionUpdateManager();

    // Create a new field definition.
    $new_field = \Drupal\Core\Field\BaseFieldDefinition::create('boolean')
      ->setLabel(t('Active'))
      ->setRevisionable(TRUE)
      ->setDescription(t('A boolean indicating whether the Need is active for volunteer purposes.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
    // Install the new definition.
    $definition_manager->installFieldStorageDefinition('active', 'vc_need', 'vc_need', $new_field);

    $entities = \Drupal::entityTypeManager()->getStorage('vc_need')->loadMultiple();
    foreach ($entities as $entity) {
      $entity->set('active', '1')->save();
    }
}

/**
 * Add "Need Status" property to report-related entities, and set to reportable initially.
 */
function vc_needs_update_8102(&$sandbox) {
  $definition_manager = \Drupal::entityDefinitionUpdateManager();

  // Create a new field definition.
  $new_field = \Drupal\Core\Field\BaseFieldDefinition::create('list_string')
    ->setLabel(t('Need Status'))
    ->setDescription(t('The current status of the need'))
    ->setRevisionable(TRUE)
    ->setSettings([
      'allowed_values' => \Drupal\vc_needs\Entity\VCNeed::getNeedStatusesAsSelectOptions(),
      'text_processing' => 0,
    ])
    ->setDefaultValue('inactive')
    ->setDisplayOptions('view', [
      'label' => 'above',
      'type' => 'string',
      'weight' => -4,
    ])
    ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => -4,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);;
  // Install the new definition.
  $definition_manager->installFieldStorageDefinition('need_status', 'vc_need', 'vc_need', $new_field);

  $entities = \Drupal::entityTypeManager()->getStorage('vc_need')->loadMultiple();
  foreach ($entities as $entity) {
    $entity->set('need_status', 'active')->save();
  }
}