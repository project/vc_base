<?php

namespace Drupal\vc_needs\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining VCNeed entities.
 *
 * @ingroup vc_needs
 */
interface VCNeedInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the VCNeed name.
   *
   * @return string
   *   Name of the VCNeed.
   */
  public function getName();

  /**
   * Sets the VCNeed name.
   *
   * @param string $name
   *   The VCNeed name.
   *
   * @return \Drupal\vc_needs\Entity\VCNeedInterface
   *   The called VCNeed entity.
   */
  public function setName($name);

  /**
   * Gets the VCNeed creation timestamp.
   *
   * @return int
   *   Creation timestamp of the VCNeed.
   */
  public function getCreatedTime();

  /**
   * Sets the VCNeed creation timestamp.
   *
   * @param int $timestamp
   *   The VCNeed creation timestamp.
   *
   * @return \Drupal\vc_needs\Entity\VCNeedInterface
   *   The called VCNeed entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the VCNeed revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the VCNeed revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\vc_needs\Entity\VCNeedInterface
   *   The called VCNeed entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the VCNeed revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the VCNeed revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\vc_needs\Entity\VCNeedInterface
   *   The called VCNeed entity.
   */
  public function setRevisionUserId($uid);

}
