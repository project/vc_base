<?php

namespace Drupal\vc_needs\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for VCNeed entities.
 */
class VCNeedViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
