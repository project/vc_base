<?php

namespace Drupal\vc_needs\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining VCNeed type entities.
 */
interface VCNeedTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
