<?php

namespace Drupal\vc_needs\Access;

use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\Routing\Route;

/**
 * Class NewVolunteer
 * @package Drupal\vc_actions_volunteer\Access
 */
class ActiveNeed implements AccessInterface {

  /**
   * Check whether the person has volunteered already.
   *
   * @param Route $route
   * @param RouteMatchInterface $route_match
   * @param AccountInterface $account
   * @return AccessResult|\Drupal\Core\Access\AccessResultAllowed
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    $vc_need = $route_match->getParameter('vc_need');
    if ($vc_need->hasField('field_active')) {
      $active = $vc_need->get('field_active')->getValue();
      $active = isset($active[0]['value']) ? $active[0]['value'] : $active;
      return AccessResult::allowedIf($active);
    }
    return AccessResult::allowed();
  }
}
