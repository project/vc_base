<?php

namespace Drupal\vc_needs;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Interface VCNeedStatusManagerInterface.
 *
 * @package Drupal\vc_needs
 */
interface VCNeedStatusManagerInterface extends PluginManagerInterface {

}
