<?php

namespace Drupal\vc_needs\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a VCNeed revision.
 *
 * @ingroup vc_needs
 */
class VCNeedRevisionDeleteForm extends ConfirmFormBase {

  /**
   * The VCNeed revision.
   *
   * @var \Drupal\vc_needs\Entity\VCNeedInterface
   */
  protected $revision;

  /**
   * The VCNeed storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $vCNeedStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->vCNeedStorage = $container->get('entity_type.manager')->getStorage('vc_need');
    $instance->connection = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vc_need_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => format_date($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.vc_need.version_history', ['vc_need' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $vc_need_revision = NULL) {
    $this->revision = $this->VCNeedStorage->loadRevision($vc_need_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->VCNeedStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')->notice('VCNeed: deleted %title revision %revision.', ['%title' => $this->revision->label(), '%revision' => $this->revision->getRevisionId()]);
    $this->messenger()->addMessage(t('Revision from %revision-date of VCNeed %title has been deleted.', ['%revision-date' => format_date($this->revision->getRevisionCreationTime()), '%title' => $this->revision->label()]));
    $form_state->setRedirect(
      'entity.vc_need.canonical',
       ['vc_need' => $this->revision->id()]
    );
    if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {vc_need_field_revision} WHERE id = :id', [':id' => $this->revision->id()])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.vc_need.version_history',
         ['vc_need' => $this->revision->id()]
      );
    }
  }

}
