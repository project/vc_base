<?php

namespace Drupal\vc_needs\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\vc_needs\Entity\VCNeedInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class VCNeedController.
 *
 *  Returns responses for VCNeed routes.
 */
class VCNeedController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a VCNeed revision.
   *
   * @param int $vc_need_revision
   *   The VCNeed revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($vc_need_revision) {
    $vc_need = $this->entityTypeManager()->getStorage('vc_need')
      ->loadRevision($vc_need_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('vc_need');

    return $view_builder->view($vc_need);
  }

  /**
   * Page title callback for a VCNeed revision.
   *
   * @param int $vc_need_revision
   *   The VCNeed revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($vc_need_revision) {
    $vc_need = $this->entityTypeManager()->getStorage('vc_need')
      ->loadRevision($vc_need_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $vc_need->label(),
      '%date' => $this->dateFormatter->format($vc_need->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a VCNeed.
   *
   * @param \Drupal\vc_needs\Entity\VCNeedInterface $vc_need
   *   A VCNeed object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(VCNeedInterface $vc_need) {
    $account = $this->currentUser();
    $vc_need_storage = $this->entityTypeManager()->getStorage('vc_need');

    $langcode = $vc_need->language()->getId();
    $langname = $vc_need->language()->getName();
    $languages = $vc_need->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $vc_need->label()]) : $this->t('Revisions for %title', ['%title' => $vc_need->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all vc_need revisions") || $account->hasPermission('administer vc_need entities')));
    $delete_permission = (($account->hasPermission("delete all vc_need revisions") || $account->hasPermission('administer vc_need entities')));

    $rows = [];

    $vids = $vc_need_storage->revisionIds($vc_need);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\vc_needs\VCNeedInterface $revision */
      $revision = $vc_need_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $vc_need->getRevisionId()) {
          $link = $this->l($date, new Url('entity.vc_need.revision', [
            'vc_need' => $vc_need->id(),
            'vc_need_revision' => $vid,
          ]));
        }
        else {
          $link = $vc_need->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.vc_need.translation_revert', [
                'vc_need' => $vc_need->id(),
                'vc_need_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.vc_need.revision_revert', [
                'vc_need' => $vc_need->id(),
                'vc_need_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.vc_need.revision_delete', [
                'vc_need' => $vc_need->id(),
                'vc_need_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['vc_need_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
